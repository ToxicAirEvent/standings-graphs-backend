//Import all of the various libaries needed to make this happen.
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

/*
	The master function of getting standings by day.
	It will grab each team, the unique set of days of games played so far, and the number of wins each team has sorted in order greatest to least.
	Sorting this way makes sure the number of wins lines up with the number of days.
*/
async function detailedStandings(league,division){
	const client = new MongoClient('mongodb://localhost:27017');

	try{
		await client.connect();

    	const db = await client.db('mlb_standings');

    	//This is all the teams and their standings data by date.
    	var grabbed_standings = await db.collection('current_standings').find({
    		"conference": league,
    		"division": division
    	}).sort({"rank": 1}).toArray();
	}catch(error){
		console.error(error);
	}finally{
		client.close();
    	console.log("Database connection should now be closed.");
	}

	return grabbed_standings;
}

module.exports.detailedStandings = detailedStandings;