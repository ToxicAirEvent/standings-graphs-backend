//Import all of the various libaries needed to make this happen.
const axios = require('axios');
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

//Define out main async function for inserting data into a mongodb.
async function main(){
  const client = new MongoClient('mongodb://localhost:27017');

  try{
    await client.connect();

    const db = await client.db('mlb_standings');

    console.log('Retrieving Standings');
    const standings_request = await retrieveStandings();

    //Create a ID for the current date in yyyymmdd format.
    let currentDate = new Date();
    let formattedMonth = ("0" + (currentDate.getMonth() + 1)).slice(-2);
    let formattedDate =  ("0" + currentDate.getDate()).slice(-2);
    let standingsDate = currentDate.getFullYear() + formattedMonth + formattedDate;

    for(const element of standings_request.data.standing){
      let standing_date_uid_value = element.team_id + "_" + standingsDate;
      element['date_of_standing'] = standingsDate;
      element['standing_date_uid'] = standing_date_uid_value;
    }

    await insertOrUpdate(db,standings_request,function(){
      console.log("For the date of: " + standingsDate);
      console.log("Data should now be inserted.");
    })

    await currentStandings(db,standings_request,function(){
      console.log("Bulk write of today most up to date standings should now be completed.");
    })

    console.log("For the date of: " + standingsDate);

  }catch (error){
    console.error(error);
  }finally{
    client.close();
    console.log("Database connection should now be closed.");
  }
}

//Axios request to grab the mlb standings from a 3rd part API by Erik Berg. Thanks Erik!
async function retrieveStandings(){
  const standings_response = await axios.get('https://erikberg.com/mlb/standings.json',
      {headers: {'User-Agent': 'Standings Graphs/1.0 (jackemceachern@gmail.com)'}});

  return await standings_response;
}

/*
  Functon to insert or update the existing data for standings. 
  Currently it just inserts if that standing for a team doesn't already exist for a given date.
  Eventually to correct data errors it should be able to update a given document but this gets us off the ground.
*/
async function insertOrUpdate(db,team_records,callback){
  const standings = await db.collection('standings_data');

  for(const single_standing_data of team_records.data.standing){
    console.log("Looping standing: " + single_standing_data.team_id);
    let teamLookup = await standings.findOne({standing_date_uid: single_standing_data.standing_date_uid}); //Eventually would be better as a bulk write of insert and updates

    if(teamLookup === null){
      await standings.insertOne(single_standing_data);
      console.log("Standing of " + single_standing_data.team_id + " inserted!");
    }

    console.log("LOOKUP OR INSERT COMPLETED.");
    console.log("---------------------------")
  }
  
  callback();
}

async function currentStandings(db,team_standings,callback){
  const current_standings = db.collection('current_standings');

  let replacePool = [];

  for(const single_team of team_standings.data.standing){
    let replaceOnePusher = {
      replaceOne: {
        "filter": {"team_id": single_team.team_id},
        "replacement": single_team
      }
    }

    replacePool.push(replaceOnePusher);
  }

  await current_standings.bulkWrite(replacePool);

  callback();
}

module.exports.CrawlStandingsAPI = main;