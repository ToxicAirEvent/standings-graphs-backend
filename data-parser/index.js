var express = require('express');
var cors = require('cors');
var app = express();
const NodeCache = require('node-cache');
var cron = require('node-cron');

const standingsCache = new NodeCache();
const dailyStandings = require('./get_standings_by_day');
const crawlStandings = require('./crawl_standings');
const detailedStandings = require('./detailed_standings');

const apiPort = 4000;

app.use(cors());

// respond with "hello world" when a GET request is made to the homepage
app.get('/', function (req, res) {
  res.send('The base pont for the API. Nothing to see here.');
})

app.get('/standings',async function(req,res){
	var standingsData;


	if(standingsCache.has('standingsByDay')){
		standingsData = await standingsCache.get('standingsByDay');
		console.log("Grabbed standings from cache.");
	}else{
		let compiledStandings = await dailyStandings.standingsByDay();
		standingsData = await JSON.stringify(compiledStandings);
		let cache_success = await standingsCache.set('standingsByDay',standingsData,1800);
		console.log("Standings cached: " + cache_success);
	}

	res.send(standingsData);
})

app.get('/current_detailed/league/:league/division/:div',async function(req,res){
	let requestedStandings = await detailedStandings.detailedStandings(req.params.league,req.params.div);
	res.send(requestedStandings);
})

app.listen(apiPort, () => console.log(`Server running on port ${apiPort}`))

//CRON jobs to check for and grab the standings each morning. If they fail, or something goes wrong retry a bit later.
var earlyCrawl = cron.schedule('0 15 3 * * *', () => {
  console.log("Crawling standings for latest data at 3:15 in the morning.");
  crawlStandings.CrawlStandingsAPI();
});

var lateCrawl = cron.schedule('0 30 6 * * *', () => {
  console.log("Crawling standings for latest data at 6:30 in the morning.");
  crawlStandings.CrawlStandingsAPI();
});

earlyCrawl.start();
lateCrawl.start();