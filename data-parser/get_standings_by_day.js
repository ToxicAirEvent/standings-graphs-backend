//Import all of the various libaries needed to make this happen.
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

/*
	The master function of getting standings by day.
	It will grab each team, the unique set of days of games played so far, and the number of wins each team has sorted in order greatest to least.
	Sorting this way makes sure the number of wins lines up with the number of days.
*/
async function standingsByDay(){
	const client = new MongoClient('mongodb://localhost:27017');

	try{
		await client.connect();

    	const db = await client.db('mlb_standings');

    	//This is all the teams and their standings data by date.
    	var all_standings = await db.collection('standings_data').find({}).toArray();

    	//Function pulls out each indivigual date that exist among/between all of the standings.
    	var indivigual_dates = await uniqueDates(all_standings);

    	//Current standings is another collection. Updated every morning that only contains the complete standings data for each team at the start of that day.
    	var currentStandings = await currentTeams(db);

    	//Once each team on their current date is zipped into complete object with their current data as well as historical data by day.
    	var teamsCompiled = [];

    	//Loop over each unique team, grab their wins by day, push that data into the complete collection of teams.
    	for(const singleTeam of currentStandings){
    		let compiledTeam = await injectWinsByDay(singleTeam,all_standings);

    		teamsCompiled.push(compiledTeam);
    	}

    	var compiledData = {dates: indivigual_dates, teamStandings: teamsCompiled};
	}catch(error){
		console.error(error);
	}finally{
		client.close();
    	console.log("Database connection should now be closed.");
	}

	return compiledData;
}

//Pull out each unique date in which standings for teams have existed.
async function uniqueDates(standingsHeap){
	let datesSet = [...new Set(standingsHeap.map(item => item.date_of_standing))];

	datesSet.sort(function(a, b) {
  		return a - b;
	});

	return datesSet;
}

//Grab all of the standings for each team in the MLB as of today.
async function currentTeams(db){
	let currentStandings = await db.collection('current_standings').find({}).toArray();

	return currentStandings;
}


//Get each teams wins over a course of days and return that team as an object with a new property of wins by day.
async function injectWinsByDay(team,completeStandings){
	let teamWins = [];

	for(const singleDayStanding of completeStandings){
		if(singleDayStanding.team_id == team.team_id){
			teamWins.push(singleDayStanding.won);
		}
	}

	teamWins.sort(function (a,b){
		return a - b;
	});

	console.log(team.team_id + " wins by day:");
	console.log(teamWins);

	team['winsByDay'] = teamWins;

	return team;
}

module.exports.standingsByDay = standingsByDay;